import React from 'react';
import { Image } from "react-native";

class Logo extends React.Component {

    logo_icon = require('../Ressources/Images logo/logo.svg').default

    render () {
        return <Image style={this.props.style.logo} source={this.logo_icon}/>
    }

}

export default Logo