import React from 'react';
import styles from './realisation.css'

function getname(i) {
    i = i+1
    const n_dig = Math.floor(Math.log10(i))+1
    const name = "0".repeat(4-n_dig) + i
    return name+".png"
}

class ImageAnim extends React.Component {

    state = {id:0,img:""}

    constructor(props) {
        super(props)

        let id = this.props.id
        let img = require("../../Ressources/Animation rock/"+getname(this.props.id)).default
        this.state = {id:id,img:img}
    }

    render () {

        let style = {opacity: 0}
        if (this.props.show) {
            let params = this.props.params
            style = {transform:"translateX(-50%) translateY("+(-params.spin_y_pos)+"em) rotateY("+(-params.spin_angle)+"deg) rotateX("+(-params.view_angle)+"deg)",
                        opacity: 1}
            style.opacity = 1 
        }
        return <img key={this.state.id} className={styles.anim_img} src={this.state.img} style={style} draggable="false"/>
    }

}

export default ImageAnim