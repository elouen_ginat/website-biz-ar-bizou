import React from 'react';
import styles from './realisation.css'

function getname(i) {
    i++
    return "image"+i+".jpg"
}

class ImageHist extends React.Component {

    state = {id:0,img:""}

    constructor(props) {
        super(props)

        let id = this.props.id
        let img = require("../../Ressources/Images histoire/"+getname(this.props.id)).default

        const params = this.props.params

        let deg = -id*params.rotation_spacing
        let z_trans = params.spiral_width;
        let y_trans = id*params.y_spacing;

        let style = {transform: "translateX(-50%) translateY("+y_trans+"px) rotateY("+deg+"deg) translateZ("+z_trans+"px)",
                        padding:0}
        
        this.state = {id:id,img:img,style:style}
    }

    render () {
        return <div key={this.state.id} className={styles.frame} style={this.state.style}>
            <div className={styles.front}>
                <img className={styles.img_front} src={this.state.img} draggable="false"/>
                <div className={styles.desc_front}>TESTSSYOVSeeeeeeee </div>
            </div>
            <div className={styles.back}>
                <img className={styles.img_back} src={this.state.img} draggable="false"/>
                <div className={styles.desc_back}>TESTSSYOVSeeeeeeee </div>
            </div>
        </div>
    }

}

export default ImageHist