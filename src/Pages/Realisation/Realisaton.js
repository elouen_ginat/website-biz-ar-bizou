import React from 'react';
import styles from './realisation.css';

import ImageAnim from './ImageAnim'
import ImageHist from './ImageHist'

//number of images
const n_img = 250
const n_img_hist = 4

//scroll scale
const scroll_scale = 1/50
//max speed
const scroll_mspeed = 100;

//first image index
let img_idx = 60

//view angle
const view_angle = 12

//spinner angle
let spin_angle = 0
//spinner y position
let spin_y_pos = 0

//spin param
const rotation_spacing = 90
const spiral_width = 300
const y_spacing = 500
const spin_speed = 4

const trans_y_speed = y_spacing/rotation_spacing/rotation_spacing*5
const max_y_pos = -y_spacing*n_img_hist/2000*104
const max_spin = -spin_speed/trans_y_speed*max_y_pos

class Realisation extends React.Component {

    constructor(props) {
        super(props)

        this.state = {img_idx:img_idx,
            params:{spin_y_pos:spin_y_pos,
                    spin_angle:spin_angle,
                    view_angle:view_angle,
                    rotation_spacing:rotation_spacing,
                    spiral_width:spiral_width,
                    y_spacing:y_spacing
            }
        }
    }

    getIdx = (amount) => {
        img_idx = this.state.img_idx
        if (Math.abs(amount) > scroll_mspeed) amount = scroll_mspeed
        img_idx = (img_idx+amount)%n_img
        if (img_idx < 0) {
            img_idx = n_img+img_idx
        }
        return img_idx
    }
    
    play_anim = (amount) => {
        let is_rotating = true
        spin_angle += spin_speed*amount
        spin_y_pos -= trans_y_speed*amount
        if (spin_angle < 0) {
            spin_angle = 0
            spin_y_pos = 0
            is_rotating = false
        }else if (spin_y_pos < max_y_pos) {
            spin_angle = max_spin
            spin_y_pos = max_y_pos
            is_rotating = false
        }
        img_idx = this.state.img_idx
        if (is_rotating) {
            img_idx = this.getIdx(amount)
        }
        console.log(img_idx)
        this.setState(
            () => {
                let prevState = {}
                Object.assign(prevState, this.state)
                prevState.img_idx = img_idx;
                prevState.params.spin_y_pos = spin_y_pos
                prevState.params.spin_angle = spin_angle
                return prevState
            })
    }
    
    onScroll = (event) => {
        const dir = (event.deltaY != 0) * (event.deltaY > 0 ? 1:-1)
        let amount = -Math.round(event.deltaY * scroll_scale) - dir
        this.play_anim(amount)
    }

    render () {

        let params = this.state.params
        let style = {transform:"rotateX("+params.view_angle+"deg) rotateY("+params.spin_angle+"deg) translateY("+params.spin_y_pos+"em)"}
        return <div onWheel={this.onScroll} className={styles.wheel_div}>
                <div className={styles.spinner} style={style}>
                    {[...Array(n_img).keys()].map(
                        (i) => <ImageAnim key={i} id={i} show={i == this.state.img_idx} params={this.state.params} />
                    )}
                    {[...Array(n_img_hist).keys()].map(
                        (i) => <ImageHist key={i} id={i} show={i == this.state.img_idx} params={this.state.params} />
                    )}
                </div>
            </div>
    }

}

export default Realisation