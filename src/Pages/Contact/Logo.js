import React from 'react';
import styles from './logo.css'

class Logo extends React.Component {

    logo_icon = require('../../Ressources/Images logo/logo.svg').default

    render () {

        return <img className={styles.logo} src={this.logo_icon}/>
    }

}

export default Logo