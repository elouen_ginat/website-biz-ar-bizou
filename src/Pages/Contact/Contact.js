import React from 'react';
import styles from './contact.css';

import Logo from '../../Logo/Logo';


class Contact extends React.Component {

    render () {
        return <div className={styles.back}>
        <div className={styles.portfoliocard}>
            <div className={styles.Head}>
                <h2 className={styles.name}>Nom</h2>
                <h3 className={styles.location}>Paris, FR</h3> 
                <Logo style={styles}/>
            </div>

       
        <div className={styles.List_contact}>
            <ul className={styles.contact_information}>
                <li className={styles.work}>Fonction</li>
    
                <li className={styles.website}><a className={styles.nostyle} href="http://monsite.com" target="_blank" rel="noreferrer">monsite.com</a></li>
        
                <li className={styles.mail}><a className={styles.nostyle} href="" target="_blank" rel="noreferrer">moi@mail.com</a></li>
        
                <li className={styles.phone}>00 00 00 00 00</li>
                <li className={styles.Adresse}><a target="_blank" rel="noreferrer" className={styles.nostyle}>Adresse</a></li>
            </ul>
        </div>
    </div>
    
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,400,300,600,700,800' rel='stylesheet' type='text/css'></link>
    <link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'></link>
    
    <link href='https://fonts.googleapis.com/css?family=Amatic+SC' rel='stylesheet' type='text/css'></link>
    </div>
    }

}

export default Contact