
import React from 'react';
import Bandeau from "./Bandeau/Bandeau"
import { BrowserRouter, Route, Switch} from "react-router-dom";
import {SafeAreaView} from "react-native"

import Bijoux from './Pages/Bijoux/Bijoux'
import Contact from './Pages/Contact/Contact'
import Credits from './Pages/Credits/Credits';
import Histoire from './Pages/Histoire/Histoire';
import Realisation from './Pages/Realisation/Realisaton';

//classe permetant de faire le lien entre les boutons et les pages
class Sheet {

  constructor(name, page) {
    this.name = name
    this.page = page
  }

  //retourne le lien associé à la page
  getPath() {
    return "/"+this.name
  }
}

class App extends React.Component {

  //variable faisant le lien entre les noms et les pages
  sheets = [new Sheet("Crédits", Credits),
            new Sheet("Contact", Contact),
            new Sheet("Histoire", Histoire),
            new Sheet("Réalisation d'une bague", Realisation),
            new Sheet("Bijoux", Bijoux)]

  //retourne les routes des pages
  getPages = () => {
    return this.sheets.map((s) => <Route key={s.getPath()} path={s.getPath()} component={s.page} />)
  }

  render() {
    return (
      <SafeAreaView>
        <BrowserRouter>
            <Bandeau sheets={this.sheets}></Bandeau>
            <Switch>
                {/*this.getPages()*/}
            </Switch> 
        </BrowserRouter>
    </SafeAreaView>
    )
  }
}

export default App;