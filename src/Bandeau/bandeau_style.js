import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    bandeau_logo: {
        display:"block",
        height:"150px"
    },
    bandeau: {
        flex:1,
        backgroundColor: "#2d3f4b",
        flexDirection: "row",
        height:"70%",
    }
});

export default styles
