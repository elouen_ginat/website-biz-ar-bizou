import React from 'react';
import styles from './pagelink_style.js'
import ButtonBand from './ButtonBand'
import {View} from "react-native"

let createButtons = (pages) => {
    return pages.map((page) =>
        <ButtonBand key={page.name} text={page.name} path={page.getPath()}></ButtonBand>
    )
}

class PageLink extends React.Component {
    render () {
        return <View style={styles.pagelink}>
            {createButtons(this.props.pages)}
        </View>
    }

}

export default PageLink