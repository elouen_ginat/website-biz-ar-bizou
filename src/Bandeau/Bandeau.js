import React from 'react';
import { View } from "react-native";
import styles from './bandeau_style.js';
import Logo from '../Logo/Logo';
import PageLink from './PageLink';
import style_logo from './logo_style.js'


class Bandeau extends React.Component {

    render () {
        return <View style={styles.bandeau_logo}>
            <Logo style={style_logo}></Logo>
            <View style={styles.bandeau}>
                <PageLink pages={this.props.sheets}></PageLink>
            </View>  
        </View>  
    }

}

export default Bandeau