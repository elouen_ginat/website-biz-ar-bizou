import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
button: {
    marginRight: "20px",
    height: "50%",
    cursor: "pointer",
    boxShadow: "5px 5px 7px 0 rgba(0, 0, 0, 0.25),-3px -3px 7px 0 rgba(255, 255, 255, 0.25),5px 5px 7px 0 rgba(0, 0, 0, 0) inset,-3px -3px 7px 0 rgba(255, 255, 255, 0) inset",         
    borderRadius: "50px",
},
text: {
    fontFamily: "Roboto,sans-serif",
    fontSize: 18,
    color: "rgb(212, 212, 212)",
    margin:10,
    alignSelf: "center",
}

});

export default styles