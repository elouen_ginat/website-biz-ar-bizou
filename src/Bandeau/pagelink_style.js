import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    pagelink: {
      flex:1,
      flexDirection: "row-reverse",
      alignItems: "center",
    } 
});

export default styles