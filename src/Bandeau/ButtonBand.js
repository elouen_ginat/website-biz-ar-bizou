import React from 'react';
import styles from './button_band_style.js'
import {NavLink} from "react-router-dom";
import {Text, TouchableOpacity} from "react-native"

let onPress = () => {

}

let link_style = {
    color: "rgba(0,0,0,0)"
}

let active_link_style = {
    fontWeight: "bold",
    color: "rgba(0,0,0,0)"
}

class ButtonBand extends React.Component {

    render () {
        return <NavLink to={this.props.path}
            activeStyle={active_link_style} 
            style={link_style}>
            <TouchableOpacity style={styles.button} 
                                activeOpacity={0.8} 
                                onPress={onPress}>
                <Text style={styles.text}>{this.props.text}</Text>
            </TouchableOpacity>
        </NavLink>
    }

}

export default ButtonBand