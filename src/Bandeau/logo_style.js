import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    logo: {
      position:"absolute",
      width: "20%",
      height:"90%",
      resizeMode:"contain",
      marginTop:"20px",
      marginLeft:"1%",
      zIndex:2
    },
});

export default styles